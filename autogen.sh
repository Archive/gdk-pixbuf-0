#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="gdk-pixbuf"

(test -f $srcdir/configure.in \
  && test -f $srcdir/README \
  && test -f $srcdir/gdk-pixbuf/gdk-pixbuf.c) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gdk-pixbuf directory"
    exit 1
}

. $srcdir/subautogen.sh
