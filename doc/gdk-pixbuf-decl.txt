<USER_FUNCTION>
<NAME>ModulePreparedNotifyFunc</NAME>
<RETURNS>void </RETURNS>
GdkPixbuf *pixbuf, gpointer user_data
</USER_FUNCTION>
<USER_FUNCTION>
<NAME>ModuleUpdatedNotifyFunc</NAME>
<RETURNS>void </RETURNS>
GdkPixbuf *pixbuf,
					  guint x, guint y,
					  guint width, guint height,
					  gpointer user_data
</USER_FUNCTION>
<USER_FUNCTION>
<NAME>ModuleFrameDoneNotifyFunc</NAME>
<RETURNS>void </RETURNS>
GdkPixbufFrame *frame,
					    gpointer user_data
</USER_FUNCTION>
<USER_FUNCTION>
<NAME>ModuleAnimationDoneNotifyFunc</NAME>
<RETURNS>void </RETURNS>
GdkPixbuf *pixbuf,
						gpointer user_data
</USER_FUNCTION>
<STRUCT>
<NAME>GdkPixbufModule</NAME>
</STRUCT>
<STRUCT>
<NAME>GdkPixbufModule</NAME>
struct GdkPixbufModule {
	char *module_name;
	gboolean (* format_check) (guchar *buffer, int size);
	GModule *module;
	GdkPixbuf *(* load) (FILE *f);
        GdkPixbuf *(* load_xpm_data) (const char **data);

        /* Incremental loading */

        gpointer (* begin_load) (ModulePreparedNotifyFunc prepare_func,
				 ModuleUpdatedNotifyFunc update_func,
				 ModuleFrameDoneNotifyFunc frame_done_func,
				 ModuleAnimationDoneNotifyFunc anim_done_func,
				 gpointer user_data);
        void (* stop_load) (gpointer context);
        gboolean (* load_increment) (gpointer context, const guchar *buf, guint size);

	/* Animation loading */
	GdkPixbufAnimation *(* load_animation) (FILE *f);
};
</STRUCT>
<FUNCTION>
<NAME>gdk_pixbuf_get_module</NAME>
<RETURNS>GdkPixbufModule  *</RETURNS>
guchar *buffer, guint size
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_load_module</NAME>
<RETURNS>void  </RETURNS>
GdkPixbufModule *image_module
</FUNCTION>
<MACRO>
<NAME>GDK_TYPE_PIXBUF_LOADER</NAME>
#define GDK_TYPE_PIXBUF_LOADER		   (gdk_pixbuf_loader_get_type ())
</MACRO>
<MACRO>
<NAME>GDK_PIXBUF_LOADER</NAME>
#define GDK_PIXBUF_LOADER(obj)		   (GTK_CHECK_CAST ((obj), GDK_TYPE_PIXBUF_LOADER, GdkPixbufLoader))
</MACRO>
<MACRO>
<NAME>GDK_PIXBUF_LOADER_CLASS</NAME>
#define GDK_PIXBUF_LOADER_CLASS(klass)	   (GTK_CHECK_CLASS_CAST ((klass), GDK_TYPE_PIXBUF_LOADER, GdkPixbufLoaderClass))
</MACRO>
<MACRO>
<NAME>GDK_IS_PIXBUF_LOADER</NAME>
#define GDK_IS_PIXBUF_LOADER(obj)	   (GTK_CHECK_TYPE ((obj), GDK_TYPE_PIXBUF_LOADER))
</MACRO>
<MACRO>
<NAME>GDK_IS_PIXBUF_LOADER_CLASS</NAME>
#define GDK_IS_PIXBUF_LOADER_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), GDK_TYPE_PIXBUF_LOADER))
</MACRO>
<STRUCT>
<NAME>GdkPixbufLoader</NAME>
</STRUCT>
<STRUCT>
<NAME>GdkPixbufLoaderClass</NAME>
</STRUCT>
<STRUCT>
<NAME>GdkPixbufLoaderPrivate</NAME>
</STRUCT>
<STRUCT>
<NAME>GdkPixbufLoader</NAME>
struct GdkPixbufLoader {
	GtkObject object;

	/* Private data */
	GdkPixbufLoaderPrivate *priv;
};
</STRUCT>
<FUNCTION>
<NAME>gdk_pixbuf_loader_get_type</NAME>
<RETURNS>GtkType  </RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_loader_new</NAME>
<RETURNS>GdkPixbufLoader     *</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_loader_write</NAME>
<RETURNS>gboolean  </RETURNS>
GdkPixbufLoader *loader,const guchar    *buf,size_t           count
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_loader_get_pixbuf</NAME>
<RETURNS>GdkPixbuf           *</RETURNS>
GdkPixbufLoader *loader
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_loader_get_animation</NAME>
<RETURNS>GdkPixbufAnimation  *</RETURNS>
GdkPixbufLoader *loader
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_loader_close</NAME>
<RETURNS>void  </RETURNS>
GdkPixbufLoader *loader
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_xlib_init</NAME>
<RETURNS>void  </RETURNS>
Display *display, int screen_num
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_xlib_init_with_depth</NAME>
<RETURNS>void  </RETURNS>
Display *display, int screen_num,int prefDepth
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_xlib_render_threshold_alpha</NAME>
<RETURNS>void  </RETURNS>
GdkPixbuf *pixbuf, Pixmap bitmap,int src_x, int src_y,int dest_x, int dest_y,int width, int height,int alpha_threshold
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_xlib_render_to_drawable</NAME>
<RETURNS>void  </RETURNS>
GdkPixbuf *pixbuf,Drawable drawable, GC gc,int src_x, int src_y,int dest_x, int dest_y,int width, int height,XlibRgbDither dither,int x_dither, int y_dither
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_xlib_render_to_drawable_alpha</NAME>
<RETURNS>void  </RETURNS>
GdkPixbuf *pixbuf,Drawable drawable,int src_x, int src_y,int dest_x, int dest_y,int width, int height,GdkPixbufAlphaMode alpha_mode,int alpha_threshold,XlibRgbDither dither,int x_dither, int y_dither
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_xlib_render_pixmap_and_mask</NAME>
<RETURNS>void  </RETURNS>
GdkPixbuf *pixbuf,Pixmap *pixmap_return,Pixmap *mask_return,int alpha_threshold
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_xlib_get_from_drawable</NAME>
<RETURNS>GdkPixbuf  *</RETURNS>
GdkPixbuf *dest,Drawable src,Colormap cmap, Visual *visual,int src_x, int src_y,int dest_x, int dest_y,int width, int height
</FUNCTION>
<STRUCT>
<NAME>XlibRgbCmap</NAME>
</STRUCT>
<STRUCT>
<NAME>XlibRgbCmap</NAME>
struct XlibRgbCmap {
  unsigned int colors[256];
  unsigned char lut[256]; /* for 8-bit modes */
};
</STRUCT>
<FUNCTION>
<NAME>xlib_rgb_init</NAME>
<RETURNS>void</RETURNS>
Display *display, Screen *screen
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_init_with_depth</NAME>
<RETURNS>void</RETURNS>
Display *display, Screen *screen, int prefDepth
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_gc_set_foreground</NAME>
<RETURNS>void</RETURNS>
GC gc, guint32 rgb
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_gc_set_background</NAME>
<RETURNS>void</RETURNS>
GC gc, guint32 rgb
</FUNCTION>
<ENUM>
<NAME>XlibRgbDither</NAME>
typedef enum
{
  XLIB_RGB_DITHER_NONE,
  XLIB_RGB_DITHER_NORMAL,
  XLIB_RGB_DITHER_MAX
} XlibRgbDither;
</ENUM>
<FUNCTION>
<NAME>xlib_draw_rgb_image</NAME>
<RETURNS>void</RETURNS>
Drawable drawable,GC gc,int x,int y,int width,int height,XlibRgbDither dith,unsigned char *rgb_buf,int rowstride
</FUNCTION>
<FUNCTION>
<NAME>xlib_draw_rgb_image_dithalign</NAME>
<RETURNS>void</RETURNS>
Drawable drawable,GC gc,int x,int y,int width,int height,XlibRgbDither dith,unsigned char *rgb_buf,int rowstride,int xdith,int ydith
</FUNCTION>
<FUNCTION>
<NAME>xlib_draw_rgb_32_image</NAME>
<RETURNS>void</RETURNS>
Drawable drawable,GC gc,int x,int y,int width,int height,XlibRgbDither dith,unsigned char *buf,int rowstride
</FUNCTION>
<FUNCTION>
<NAME>xlib_draw_gray_image</NAME>
<RETURNS>void</RETURNS>
Drawable drawable,GC gc,int x,int y,int width,int height,XlibRgbDither dith,unsigned char *buf,int rowstride
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_cmap_new</NAME>
<RETURNS>XlibRgbCmap  *</RETURNS>
guint32 *colors, int n_colors
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_cmap_free</NAME>
<RETURNS>void</RETURNS>
XlibRgbCmap *cmap
</FUNCTION>
<FUNCTION>
<NAME>xlib_draw_indexed_image</NAME>
<RETURNS>void</RETURNS>
Drawable drawable,GC gc,int x,int y,int width,int height,XlibRgbDither dith,unsigned char *buf,int rowstride,XlibRgbCmap *cmap
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_ditherable</NAME>
<RETURNS>Bool</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_set_verbose</NAME>
<RETURNS>void</RETURNS>
Bool verbose
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_set_install</NAME>
<RETURNS>void</RETURNS>
Bool install
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_set_min_colors</NAME>
<RETURNS>void</RETURNS>
int min_colors
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_get_cmap</NAME>
<RETURNS>Colormap</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_get_visual</NAME>
<RETURNS>Visual  *</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_get_visual_info</NAME>
<RETURNS>XVisualInfo  *</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_get_depth</NAME>
<RETURNS>int</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_get_display</NAME>
<RETURNS>Display  *</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>xlib_rgb_get_screen</NAME>
<RETURNS>Screen  *</RETURNS>
void
</FUNCTION>
<ENUM>
<NAME>GdkColorspace</NAME>
typedef enum {
	GDK_COLORSPACE_RGB
} GdkColorspace;
</ENUM>
<STRUCT>
<NAME>GdkPixbuf</NAME>
</STRUCT>
<STRUCT>
<NAME>GdkPixbufFrame</NAME>
</STRUCT>
<STRUCT>
<NAME>GdkPixbufAnimation</NAME>
</STRUCT>
<USER_FUNCTION>
<NAME>GdkPixbufDestroyNotify</NAME>
<RETURNS>void </RETURNS>
guchar *pixels, gpointer data
</USER_FUNCTION>
<USER_FUNCTION>
<NAME>GdkPixbufLastUnref</NAME>
<RETURNS>void </RETURNS>
GdkPixbuf *pixbuf, gpointer data
</USER_FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_ref</NAME>
<RETURNS>GdkPixbuf  *</RETURNS>
GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_unref</NAME>
<RETURNS>void  </RETURNS>
GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_set_last_unref_handler</NAME>
<RETURNS>void  </RETURNS>
GdkPixbuf          *pixbuf,GdkPixbufLastUnref  last_unref_fn,gpointer            last_unref_fn_data
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_finalize</NAME>
<RETURNS>void  </RETURNS>
GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_get_colorspace</NAME>
<RETURNS>GdkColorspace  </RETURNS>
const GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_get_n_channels</NAME>
<RETURNS>int  </RETURNS>
const GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_get_has_alpha</NAME>
<RETURNS>gboolean  </RETURNS>
const GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_get_bits_per_sample</NAME>
<RETURNS>int  </RETURNS>
const GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_get_pixels</NAME>
<RETURNS>guchar        *</RETURNS>
const GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_get_width</NAME>
<RETURNS>int  </RETURNS>
const GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_get_height</NAME>
<RETURNS>int  </RETURNS>
const GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_get_rowstride</NAME>
<RETURNS>int  </RETURNS>
const GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_new</NAME>
<RETURNS>GdkPixbuf  *</RETURNS>
GdkColorspace colorspace, gboolean has_alpha, int bits_per_sample,int width, int height
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_copy</NAME>
<RETURNS>GdkPixbuf  *</RETURNS>
const GdkPixbuf *pixbuf
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_new_from_file</NAME>
<RETURNS>GdkPixbuf  *</RETURNS>
const char *filename
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_new_from_data</NAME>
<RETURNS>GdkPixbuf  *</RETURNS>
const guchar *data,GdkColorspace colorspace,gboolean has_alpha,int bits_per_sample,int width, int height,int rowstride,GdkPixbufDestroyNotify destroy_fn,gpointer destroy_fn_data
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_new_from_xpm_data</NAME>
<RETURNS>GdkPixbuf  *</RETURNS>
const char **data
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_add_alpha</NAME>
<RETURNS>GdkPixbuf  *</RETURNS>
const GdkPixbuf *pixbuf, gboolean substitute_color,guchar r, guchar g, guchar b
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_copy_area</NAME>
<RETURNS>void  </RETURNS>
const GdkPixbuf *src_pixbuf,int src_x, int src_y,int width, int height,GdkPixbuf *dest_pixbuf,int dest_x, int dest_y
</FUNCTION>
<ENUM>
<NAME>GdkPixbufAlphaMode</NAME>
typedef enum {
	GDK_PIXBUF_ALPHA_BILEVEL,
	GDK_PIXBUF_ALPHA_FULL
} GdkPixbufAlphaMode;
</ENUM>
<FUNCTION>
<NAME>gdk_pixbuf_render_threshold_alpha</NAME>
<RETURNS>void  </RETURNS>
GdkPixbuf *pixbuf, GdkBitmap *bitmap,int src_x, int src_y,int dest_x, int dest_y,int width, int height,int alpha_threshold
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_render_to_drawable</NAME>
<RETURNS>void  </RETURNS>
GdkPixbuf *pixbuf,GdkDrawable *drawable, GdkGC *gc,int src_x, int src_y,int dest_x, int dest_y,int width, int height,GdkRgbDither dither,int x_dither, int y_dither
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_render_to_drawable_alpha</NAME>
<RETURNS>void  </RETURNS>
GdkPixbuf *pixbuf, GdkDrawable *drawable,int src_x, int src_y,int dest_x, int dest_y,int width, int height,GdkPixbufAlphaMode alpha_mode,int alpha_threshold,GdkRgbDither dither,int x_dither, int y_dither
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_render_pixmap_and_mask</NAME>
<RETURNS>void  </RETURNS>
GdkPixbuf *pixbuf,GdkPixmap **pixmap_return, GdkBitmap **mask_return,int alpha_threshold
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_get_from_drawable</NAME>
<RETURNS>GdkPixbuf  *</RETURNS>
GdkPixbuf *dest,GdkDrawable *src, GdkColormap *cmap,int src_x, int src_y,int dest_x, int dest_y,int width, int height
</FUNCTION>
<ENUM>
<NAME>GdkInterpType</NAME>
typedef enum {
	GDK_INTERP_NEAREST,
	GDK_INTERP_TILES,
	GDK_INTERP_BILINEAR,
	GDK_INTERP_HYPER
} GdkInterpType;
</ENUM>
<FUNCTION>
<NAME>gdk_pixbuf_scale</NAME>
<RETURNS>void  </RETURNS>
const GdkPixbuf *src,GdkPixbuf       *dest,int              dest_x,int              dest_y,int              dest_width,int              dest_height,double           offset_x,double           offset_y,double           scale_x,double           scale_y,GdkInterpType    interp_type
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_composite</NAME>
<RETURNS>void  </RETURNS>
const GdkPixbuf *src,GdkPixbuf       *dest,int              dest_x,int              dest_y,int              dest_width,int              dest_height,double           offset_x,double           offset_y,double           scale_x,double           scale_y,GdkInterpType    interp_type,int              overall_alpha
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_composite_color</NAME>
<RETURNS>void  </RETURNS>
const GdkPixbuf *src,GdkPixbuf       *dest,int              dest_x,int              dest_y,int              dest_width,int              dest_height,double           offset_x,double           offset_y,double           scale_x,double           scale_y,GdkInterpType    interp_type,int              overall_alpha,int              check_x,int              check_y,int              check_size,guint32          color1,guint32          color2
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_scale_simple</NAME>
<RETURNS>GdkPixbuf  *</RETURNS>
const GdkPixbuf *src,int              dest_width,int              dest_height,GdkInterpType    interp_type
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_composite_color_simple</NAME>
<RETURNS>GdkPixbuf  *</RETURNS>
const GdkPixbuf *src,int              dest_width,int              dest_height,GdkInterpType    interp_type,int              overall_alpha,int              check_size,guint32          color1,guint32          color2
</FUNCTION>
<ENUM>
<NAME>GdkPixbufFrameAction</NAME>
typedef enum {
	GDK_PIXBUF_FRAME_RETAIN,
	GDK_PIXBUF_FRAME_DISPOSE,
	GDK_PIXBUF_FRAME_REVERT
} GdkPixbufFrameAction;
</ENUM>
<FUNCTION>
<NAME>gdk_pixbuf_animation_new_from_file</NAME>
<RETURNS>GdkPixbufAnimation  *</RETURNS>
const char         *filename
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_animation_ref</NAME>
<RETURNS>GdkPixbufAnimation  *</RETURNS>
GdkPixbufAnimation *animation
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_animation_unref</NAME>
<RETURNS>void  </RETURNS>
GdkPixbufAnimation *animation
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_animation_get_width</NAME>
<RETURNS>int  </RETURNS>
GdkPixbufAnimation *animation
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_animation_get_height</NAME>
<RETURNS>int  </RETURNS>
GdkPixbufAnimation *animation
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_animation_get_frames</NAME>
<RETURNS>GList               *</RETURNS>
GdkPixbufAnimation *animation
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_animation_get_num_frames</NAME>
<RETURNS>int  </RETURNS>
GdkPixbufAnimation *animation
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_frame_get_pixbuf</NAME>
<RETURNS>GdkPixbuf            *</RETURNS>
GdkPixbufFrame *frame
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_frame_get_x_offset</NAME>
<RETURNS>int  </RETURNS>
GdkPixbufFrame *frame
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_frame_get_y_offset</NAME>
<RETURNS>int  </RETURNS>
GdkPixbufFrame *frame
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_frame_get_delay_time</NAME>
<RETURNS>int  </RETURNS>
GdkPixbufFrame *frame
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_frame_get_action</NAME>
<RETURNS>GdkPixbufFrameAction  </RETURNS>
GdkPixbufFrame *frame
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_preinit</NAME>
<RETURNS>void  </RETURNS>
gpointer app, gpointer modinfo
</FUNCTION>
<FUNCTION>
<NAME>gdk_pixbuf_postinit</NAME>
<RETURNS>void  </RETURNS>
gpointer app, gpointer modinfo
</FUNCTION>
<MACRO>
<NAME>GNOME_TYPE_CANVAS_PIXBUF</NAME>
#define GNOME_TYPE_CANVAS_PIXBUF            (gnome_canvas_pixbuf_get_type ())
</MACRO>
<MACRO>
<NAME>GNOME_CANVAS_PIXBUF</NAME>
#define GNOME_CANVAS_PIXBUF(obj)            (GTK_CHECK_CAST ((obj),		\
					     GNOME_TYPE_CANVAS_PIXBUF, GnomeCanvasPixbuf))
</MACRO>
<MACRO>
<NAME>GNOME_CANVAS_PIXBUF_CLASS</NAME>
#define GNOME_CANVAS_PIXBUF_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass),	\
					     GNOME_TYPE_CANVAS_PIXBUF, GnomeCanvasPixbufClass))
</MACRO>
<MACRO>
<NAME>GNOME_IS_CANVAS_PIXBUF</NAME>
#define GNOME_IS_CANVAS_PIXBUF(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_PIXBUF))
</MACRO>
<MACRO>
<NAME>GNOME_IS_CANVAS_PIXBUF_CLASS</NAME>
#define GNOME_IS_CANVAS_PIXBUF_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass),	\
					     GNOME_TYPE_CANVAS_PIXBUF))
</MACRO>
<STRUCT>
<NAME>GnomeCanvasPixbuf</NAME>
</STRUCT>
<STRUCT>
<NAME>GnomeCanvasPixbufClass</NAME>
</STRUCT>
<STRUCT>
<NAME>GnomeCanvasPixbuf</NAME>
struct GnomeCanvasPixbuf {
	GnomeCanvasItem item;

	/* Private data */
	gpointer priv;
};
</STRUCT>
<FUNCTION>
<NAME>gnome_canvas_pixbuf_get_type</NAME>
<RETURNS>GtkType  </RETURNS>
void
</FUNCTION>
<MACRO>
<NAME>GDK_PIXBUF_MAJOR</NAME>
#define GDK_PIXBUF_MAJOR (0)
</MACRO>
<MACRO>
<NAME>GDK_PIXBUF_MINOR</NAME>
#define GDK_PIXBUF_MINOR (21)
</MACRO>
<MACRO>
<NAME>GDK_PIXBUF_MICRO</NAME>
#define GDK_PIXBUF_MICRO (0)
</MACRO>
<MACRO>
<NAME>GDK_PIXBUF_VERSION</NAME>
#define GDK_PIXBUF_VERSION "0.21.0"
</MACRO>
<VARIABLE>
<NAME>gdk_pixbuf_version</NAME>
extern const char *gdk_pixbuf_version;
</VARIABLE>
